import Chart from 'chart.js/auto';
import '@/styles/index.scss'

$(function(){
    UsageDashboard.Init();
  });
  
  const UsageDashboard = {
    "ChartData": [], //ALL CHART DATA
    "PostTypes": [], //LIST OF ALL THE MAIN POST TYPES (updates in GetChartData)
    "PostsPerType": [], //CORRESPONDING NUMBERS OF POSTS PER MAIN POST TYPE (updates in BuildChartArrs)
    "PostSubTypes": [], //LIST OF ALL THE POST SUBTYPES (updates in BuildChartArrs)
    "PostsPerSubType": [], //CORRESPONDING NUMBERS OF POSTS PER SUBTYPE (updates in BuildChartArrs)
    "SortedPostSubTypes": [], //LIST OF ALL THE POST SUBTYPES ALPHA SORTED (updates in GetChartData)
    "SortedPostsPerSubType": [], //CORRESPONDING NUMBERS OF POSTS PER SUBTYPE TO MATCH ALPHA SORTING (updates in GetChartData)
    "DetailChartArgs": [0, "#CA6DA6", "Office"], //ARGS FOR UPDATING THE DETAIL CHART (updates in DistributionChart)
    "ChartColors": ["#CA6DA6", "#91b24d", "#7BC3F1", "#FCC266"], //DEFAULT CHART COLORS

    "Init": function(){
      this.BuildTabs();
      this.TabInteractions();
      this.AuthorsInfo();
    },
  
    "GetChartData": async function(){
        //CONSUME MOCK DATA API
        //const apiUrl = "http://localhost:8080/data/chart-data.json";
        const apiUrl = "https://bbadam.gitlab.io/usage-dashboard/public/data/chart-data.json";
        const response = await fetch(apiUrl);
        const chartData = await response.json();
  
        //SORT DATA BY NUMBER OF POSTS FOR USE IN TOP AUTHORS CHART
        UsageDashboard.ChartData = chartData.data.sort(function (a, b) {
          return b.num_posts  - a.num_posts ;
        });
  
        //LOOP THROUGH ALL AUTHORS AND BUILD AN ARRAY OF POST TYPES FOR USE IN DistributionChart
        $.each(UsageDashboard.ChartData, function(index, item){

          let postType = item.post_type;
          postType = postType.trim();
          let postSubType = item.post_subtype;
          postSubType = postSubType.trim();
          let postNum = item.num_posts;
  
          if(postType != ""){ //MAKE SURE THERE ACTUALLY IS A POST TYPE SET
            //IF THE POST TYPE DOESNT ALREADY EXIST IN THE ARRAY OF ALL POST TYPES, PUSH IT THERE
            UsageDashboard.PostTypes.indexOf(postType) === -1 ? UsageDashboard.PostTypes.push(postType) : "";

            //FIND OUT WHERE THIS POST TYPE LIVES IN THE POST TYPES ARRAY TO KEEP THINGS IN SYNC
            let arrPos = UsageDashboard.PostTypes.indexOf(postType);

            //UPDATE THE PROPER ARRAYS IN THE MAIN OBJECT WITH POSTS PER TYPE, SUBPOSTS PER TYPE, ETC
            UsageDashboard.BuildChartArrs(arrPos, postNum, postSubType);
          }
        });    

        //SORT LABELS AND DATA ALPHABETICALLY FOR BAR CHART DISPLAY
        $.each(UsageDashboard.PostSubTypes, function(index, item) {

          const origLabels = item;
          const alphaLabels = origLabels.slice();
          alphaLabels.sort();

          //PUT THE NEWLY SORTED LABELS SOMEWHERE THAT DetailChart CAN ACCESS IT
          UsageDashboard.SortedPostSubTypes.push(alphaLabels.sort());

          const origData = UsageDashboard.PostsPerSubType[index];
          let labelCounter;
  
          //MATCH DATA TO SORTED LABELS
          for(labelCounter = 0; labelCounter < origLabels.length; labelCounter++){
            let origPos = origLabels.indexOf(alphaLabels[labelCounter]);

            if (!Array.isArray(UsageDashboard.SortedPostsPerSubType[labelCounter])) { //FIRST TIME, CREATE AN EMPTY ARRAY
              UsageDashboard.SortedPostsPerSubType.push([]);
            }
            //PUT THE NEWLY SORTED DATA SOMEWHERE THAT DetailChart CAN ACCESS IT
            UsageDashboard.SortedPostsPerSubType[index].push(origData[origPos]);
          }
        }); 
    },
  
    "BuildChartArrs": function(arrPos, postNum, postSubType){
  
      let totalPosts = 0;
      let totalSubTypes = 0;
      let postMarker = arrPos;
  
      //CHECK TO SEE IF THIS IS THE FIRST TIME WEVE SEEN THIS POST TYPE
      if(UsageDashboard.PostsPerType[arrPos] == NaN || UsageDashboard.PostsPerType[arrPos] == undefined) { //FIRST TIME, SET POST TO 0
        totalPosts = 0;
      } else { //THERE HAS ALREADY BEEN A POST(S) OF THIS TYPE, GRAB THE CURRENT COUNT
        totalPosts = UsageDashboard.PostsPerType[arrPos];
      }
      postMarker = arrPos;
  
      //CHECK THROUGH DATA TO FIND MATCHES TO POST SUBTYPE
      const matchedTypes = UsageDashboard.ChartData.filter(item=> item['post_subtype'] === postSubType);
      const myMatch = matchedTypes[0].post_subtype; //myMatch IS THE CURRENT POST SUBTYPE THAT WE ARE LOOKING AT
  
      //CHECK TO SEE IF THE IS THE FIRST TIME WEVE SEEN THIS POST SUBTYPE
      if (!Array.isArray(UsageDashboard.PostSubTypes[arrPos])) { //FIRST TIME, CREATE AN EMPTY ARRAY
        UsageDashboard.PostSubTypes.push([]);
      }
  
      //WHILE WE ARE ALREADY LOOPING THROUGH THE DATA TO GATHER MAIN POST TYPES, WE MIGHT AS WELL ALSO GATHER POST SUBTYPE DATA
      UsageDashboard.PostSubTypes[arrPos].indexOf(myMatch) === -1 ? updateArrays() : "";
  
      function updateArrays(){
        UsageDashboard.PostSubTypes[arrPos].push(myMatch);
  
        //LOOP THROUGH EACH AUTHOR FOR THIS SUBTYPE AND GATHER POST NUMBERS
        $.each(matchedTypes, function(index, item){
          totalSubTypes += item.num_posts;
        })
  
        //CHECK TO SEE IF THE IS THE FIRST TIME WEVE SEEN THIS POST SUBTYPE
        if (!Array.isArray(UsageDashboard.PostsPerSubType[arrPos])) { //FIRST TIME, CREATE AN EMPTY ARRAY
          UsageDashboard.PostsPerSubType.push([]);
        }
        //UPDATE THE COUNT FOR THE TOTAL POSTS FOR THIS SUBTYPE
        UsageDashboard.PostsPerSubType[arrPos].push(totalSubTypes);
      }

      //UPDATE THE COUNT FOR THE TOTAL POSTS FOR THIS POST TYPE
      postNum = totalPosts + postNum;
      UsageDashboard.PostsPerType[postMarker] = postNum;

    },

    "BuildTabs": async function(){

      try {
        const chartData = await chartDataPromise;
        createTabsHTML();
      } catch (error) {
        console.log("BuildTabs: There is no chart data! "+error);
      }

      function createTabsHTML(){

        let tabsHTML = "<div class='chart-wrapper'><canvas id='chart-distribution' tabindex='0' role='img'></canvas></div><div role='tablist' class='chart-tabs' aria-labelledby='post-distribution-tablist'>";
        let tabPanelsHTML = "";
        let tabSelected = "true";
        let tabIndex = "0";
        let tabClass = "active";

        //BUILD TABS
        $.each(UsageDashboard.PostTypes, function(index, item){
          if(index != 0){
            tabSelected = "false";
            tabIndex = "-1";
            tabClass = "";
          } 
          tabsHTML += "<button data-content='"+ item +"' class='tab-item "+ tabClass +"' tabindex='"+ tabIndex +"' id='dist-tab-"+ index +"' role='tab' aria-controls='dist-tab-panel-"+ index +"' aria-selected='"+ tabSelected +"'>"+ UsageDashboard.PostsPerType[index] +" "+ item +"</button>";
          tabPanelsHTML += "<div class='tab-panel "+ tabClass +"' id='dist-tab-panel-"+ index +"' role='tabpanel' tabindex='0' aria-labelledby='dist-tab-"+ index +"'><h2>BY "+ item +"</h2><div class='chart-wrapper'><canvas id='dist-tab-panel-"+ index +"-chart' role='img' tabindex='0'></canvas></div></div>";
        })

        tabsHTML += "</div>";

        //PLACE TABS AND PANELS INTO THE DOM
        $("#chart-tabs-wrapper").append(tabsHTML);
        $(".usage-column.detail").append(tabPanelsHTML);
       
        //DRAW DISTRIBUTION AND DETAIL CHARTS
        UsageDashboard.DistributionChart();
        UsageDashboard.DetailChart();
      }
    },

    "TabInteractions": function(){
      //CHANGE ACTIVE PANEL ON TAB FOCUS
      $("#chart-tabs-wrapper").on("focus", ".tab-item", function(){
        if(!$(this).hasClass("active")){ //ONLY UPDATE IF AN INACTIVE TAB IS FOCUSED
              //RESET ALL TABS
              $(".tab-item").attr({
                  "aria-selected":"false",
                  "tabindex":"-1"
              }).removeClass("active");

              //FIND OUT WHICH PANEL THIS TAB CONTROLS
              const tabPanel = $(this).attr("aria-controls");

              //ACTIVATE TAB
              $(this).attr({
                  "aria-selected":"true",
                  "tabindex":"0"
              }).addClass("active");

              //RESET ALL PANELS
              $(".tab-panel").removeClass("active");

              //ACTIVATE PANEL
              $("#"+tabPanel).addClass("active");
              
              //UPDATE CHART DETAILS BASED ON ACTIVE TAB FOR CHART DRAWING
              UsageDashboard.DetailChartArgs = [$(this).index(), UsageDashboard.ChartColors[$(this).index()], $(this).attr("data-content")];

              //IN ORDER TO KEEP THE CHART ANIMATION WHEN WE SWITCH BACK TO A TAB WEVE ALREADY SEEN, WE NEED TO DESTROY THE CANVAS AND REDRAW
              $(".tab-panel.active").find("canvas").remove();

              //ADD A NEW CANVAS TO THE DOM WITH THE SAME ATTRIBUTES AS THE OLD
              $(".tab-panel.active").find(".chart-wrapper").html("<canvas id='dist-tab-panel-"+ $(this).index() +"-chart' role='img' tabindex='0'></canvas>");

              //DRAW CHART AND UPDATE AUTHOR INFO
              UsageDashboard.DetailChart();
              UsageDashboard.AuthorsInfo();
        }
      });

      //TAB KEYBOARD INTERACTIONS
      $("#chart-tabs-wrapper").on("keydown", ".tab-item", function(e) {
        //CAPTURE KEY CODE
        switch(e.key) {
            //CONSUME LEFT AND UP ARROWS
            case "ArrowLeft":
            case "ArrowUp":
                e.preventDefault();

                //THIS TAB IS THE FIRST TAB
                if($(this).is(":first-child")) {
                    //FOCUS LAST TAB
                    $(".tab-item:last-child").trigger("focus");
                } else {
                    //FOCUS PREVIOUS TAB
                    $(this).prev().trigger("focus");
                }
            break;

            //CONSUME RIGHT AND DOWN ARROWS
            case "ArrowRight":
            case "ArrowDown":
                e.preventDefault();

                //THIS TAB IS THE LAST TAB
                if($(this).is(":last-child")) {
                    //FOCUS FIRST TAB
                    $(".tab-item:first-child").trigger("focus");
                } else {
                    //FOCUS NEXT TAB
                    $(this).next().trigger("focus");
                }
            break;

            // CONSUME HOME KEY
            case "Home":
                e.preventDefault();

                // FOCUS FIRST TAB
                $(".tab-item:first-child").trigger("focus");
            break;

            // CONSUME END KEY
            case "End":
                e.preventDefault();

                // FOCUS LAST TAB
                $(".tab-item:last-child").trigger("focus");
            break;
        }
      });
    },
  
    "DistributionChart": async function(){
      //MAKE SURE API DATA HAS BEEN FETCHED BEFORE TRYING TO BUILD THE CHART
      try {
        const chartData = await chartDataPromise;
        buildChart();
      } catch (error) {
        console.log("DistributionChart: There is no chart data! "+error);
      }
  
      function buildChart(){
        const ctx = $('#chart-distribution');
            
        new Chart(ctx, {
          type: 'doughnut',
          data: {
            labels: UsageDashboard.PostTypes,
            datasets: [{
              label: '# of Posts',
              data: UsageDashboard.PostsPerType,
              borderWidth: 0,
              backgroundColor: UsageDashboard.ChartColors,
              borderColor: UsageDashboard.ChartColors
            }]
          },
          options: {
            plugins: {
              legend: {
                  display: false
              } 
            },
            tooltips: {
              mode: 'label',
            },
            animation: {
              onComplete: function(){
                //BUILD ARIA LABEL
                let ariaDetails = "";
                let endDetail = ", ";
                const numPostTypes = UsageDashboard.PostTypes.length - 1;
                $.each(UsageDashboard.PostTypes, function(index, item){
                  numPostTypes == index ? endDetail = "." : "";
                  ariaDetails += item + ": " + UsageDashboard.PostsPerType[index] + endDetail;
                });

                $(ctx).attr("aria-label", "Donut Chart showing post distribution by post type. " + ariaDetails);
              }
            }
          }
        });
      }   
    },
  
    "DetailChart": async function(){
      //MAKE SURE API DATA HAS BEEN FETCHED BEFORE TRYING TO BUILD THE CHART
      try {
        const chartData = await chartDataPromise;
        buildChart();
      } catch (error) {
        console.log("DetailChart: There is no chart data! "+error);
      }
  
      function buildChart(){
        //RELY ON UsageDashboard.DetailChartArgs TO KNOW WHICH CHART TO BUILD AND WHERE TO BUILD IT
        const ctx = $("#dist-tab-panel-"+UsageDashboard.DetailChartArgs[0].toString()+"-chart");

        if($(ctx).attr("data-rendered") != "true"){
          new Chart(ctx, {
            type: 'bar',
            data: {
              labels: UsageDashboard.SortedPostSubTypes[UsageDashboard.DetailChartArgs[0]],
              datasets: [{
                label: '# of Posts',
                data: UsageDashboard.SortedPostsPerSubType[UsageDashboard.DetailChartArgs[0]],
                borderWidth: 0,
                backgroundColor: UsageDashboard.DetailChartArgs[1],
                borderColor: ["#CA6DA6", "#91b24d", "#7BC3F1", "#FCC266"]
              }]
            },
            options: {
              scales: {
                x: {
                  display: true,
                  ticks: {
                    maxRotation: 90,
                    minRotation: 90,
                    display: true
                  }
                }
              },
              maintainAspectRatio: false,
              plugins: {
                legend: {
                    display: false
                }
              },
              animation: {
                duration: 1800,
                onComplete: function(){

                  //BUILD ARIA LABEL
                  let ariaDetails = "";
                  let endDetail = ", ";
                  const numPostTypes = UsageDashboard.SortedPostSubTypes.length - 1;
                  const postSubType = UsageDashboard.SortedPostTypes[UsageDashboard.DetailChartArgs[0]];

                  $.each(UsageDashboard.SortedPostSubTypes[UsageDashboard.DetailChartArgs[0]], function(index, item){
                    numPostTypes == index ? endDetail = "." : "";
                    ariaDetails += item + ": " + UsageDashboard.SortedPostsPerSubType[UsageDashboard.DetailChartArgs[0]][index] + endDetail;
                  });

                  $(ctx).attr({
                    "aria-label": "Bar Chart showing post distribution by " + postSubType + ". " + ariaDetails,
                    "data-rendered": "true"
                  });
                }
              }
            }
          });
        }
      }     
    },
  
    "AuthorsInfo": async function(){
      //MAKE SURE API DATA HAS BEEN FETCHED BEFORE TRYING TO UPDATE THE AUTHORS INFO
      try {
        const chartData = await chartDataPromise;
        buildAuthors();
      } catch (error) {
        console.log("AuthorsInfo: There is no chart data! "+error);
      }
  
      function buildAuthors(){
        //RELY ON UsageDashboard.DetailChartArgs TO KNOW WHICH DATA TO BUILD AUTHORS FOR
        const postType = UsageDashboard.DetailChartArgs[2];
        let authorHTML = ""; 
        let srContent = ""; 
        let endDetail = ", ";
        let numAuthors = 0;

        //CHECK THROUGH DATA TO FIND MATCHES TO SUBTYPE
        const matchedTypes = UsageDashboard.ChartData.filter(item=> item['post_type'] === postType);

        //GET A STARTING POINT FOR CIRCLE SIZE
        let sizeMultiplier = 0;

        //LOOP THROUGH EACH AUTHOR FOR THIS SUBTYPE
        $.each(matchedTypes, function(index, item){
          //JUST GRAB TOP 5
          if(index <= 4){
            let baseSize = item.num_posts;
            index == 4 ? endDetail = "." : "";

            //CHECK TO SEE IF THIS IS THE FIRST AUTHOR, IF SO THEIR POST NUMBER WILL BE OUR SCALE REFERENCE FOR CIRCLE SIZE
            if(index == 0){ //FIRST AUTHOR
                sizeMultiplier = 100 / baseSize;
            } 

            //PROPORTIONATELY SCALE THE CIRCLE SIZE
            let circleSize = sizeMultiplier * baseSize;

            //BUILD AUTHOR HTML
            authorHTML += "<li class='author-detail'>"+
            "<div class='post-circle-outer'>"+
                            "<div class='post-circle'>"+
                                "<div class='post-circle-inner' style='width:"+circleSize+"%; height:"+circleSize+"%;' ><span>"+item.num_posts+"</span></div>"+
                            "</div>"+
                            "</div>"+
                            "<div class='author-info'>"+
                              "<span class='author-name'>"+item.author_name+"</span>"+
                              "<span class='post-subtype'>"+item.post_subtype+"</div>"+
                            "</li>";
            srContent += item.author_name+": "+item.num_posts+" posts" + endDetail;

            numAuthors = index+1;           
          }
        });
        
        //REMOVE LOADED CLASS FOR EXIT TRANSITIONS
        $(".usage-authors").removeClass("loaded");
        
        setTimeout(function(){
          //WHILE THE AUTHORS ARE INVISIBLE, UPDATE THEIR DATA
          $(".author-count").html("<p>"+matchedTypes.length+"<span>AUTHORS</span></p>");
          $(".usage-authors").attr("data-class", postType).find(".author-details").html(authorHTML);
        }, 500);

        setTimeout(function(){
          //ADD LOADED CLASS FOR ENTRANCE TRANSITIONS
          $(".usage-authors").addClass("loaded");
        }, 700);

        //UPDATE FOR SCREEN READERS TO ANNOUNCE NEW CONTENT TO THE DOM
        $(".sr-only").text("Showing the top "+ numAuthors +" of "+matchedTypes.length+" total authors in the "+matchedTypes[0].post_type+" category. "+srContent);
      }
    }
}

//SET UP A PROMISE THAT OTHER FUNCTIONS WILL CHECK BEFORE THEY RUN TO MAKE SURE DATA HAS BEEN RETRIEVED
const chartDataPromise = UsageDashboard.GetChartData();

